FROM adoptopenjdk/openjdk11:alpine-jre
COPY target/company.0.1.jar company.0.1.jar
WORKDIR /opt/app
ENTRYPOINT ["java","-jar","/company.0.1.jar"]
EXPOSE 8081